﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("todos")]
    public class TodosController : ControllerBase
    {
        // GET api/values
        /*[HttpGet]
       public ActionResult<IEnumerable<string>> Get()
       {
           MysqlTodoRepository repo = new MysqlTodoRepository();
           repo.SelectTodoItem();
           //return new string[] { "ssssss", "value2" };
       }*/

        [HttpGet]
        public ActionResult<IEnumerable<TodoItem>> get() {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            return repo.SelectTodoItem();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<TodoItem> Get(int id)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            return repo.BuscarTodoItem(id);
        }
        

       
        // POST api/values
        [HttpPost]
        public void Post( [FromBody] TodoItem value)
        { 
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.CrearTodoItem(value);

        }
        [HttpPost]
        [Route("nuevoUsuario")]
        public void NuevoUsuario([FromBody] acceso datoUsuario)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.CrearUsuario(datoUsuario);

        }

        [HttpGet]
        [Route("acceso")]
        public void loguin(string usuario, string pass)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.LoginUser(usuario, pass);

        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody] TodoItem valores)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.UpdateTodoItem(valores);
        }

        [HttpPut]

        [Route("CambiarEstado")]
        public void CambiarEstado([FromBody] TodoItem valores)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.UpdateStatusTodoItem(valores.id, valores.estado);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            MysqlTodoRepository repo = new MysqlTodoRepository();
            repo.EliminarTodoItem(id);
        }
        

    }
}
