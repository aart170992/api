﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Model
{
    public class MysqlTodoRepository
    {
        MySQLConection con;

        public MysqlTodoRepository() {
            con = new MySQLConection();
        }
        //crear usuario 
        public void CrearUsuario(acceso datoUsuario)
        {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");

            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"INSERT INTO `usuario` ( `usuario`, `pass_usuario`, `nombre_usuario`, `email_usuario`)  VALUES (\"{datoUsuario.usuario}\",\"{datoUsuario.pass}\",\"{datoUsuario.nombres}\",\"{datoUsuario.email}\") ";
            
            cmd.ExecuteNonQuery();
        }

        public acceso LoginUser(string usuario, string pass)
        {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");
            acceso loguin = new acceso();
            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"SELECT* FROM `usuario` where ( `usuario`=\"{usuario}\" and `pass_usuario`=\"{pass}\") ";
            MySqlDataReader reader = cmd.ExecuteReader();
           
                while (reader.Read())
                {
                    loguin.id_usuario = int.Parse(reader["id_usuario"].ToString());
                    loguin.usuario = reader["usuario"].ToString();
                    loguin.pass = reader["pass_usuario"].ToString();
                    loguin.nombres = reader["nombre_usuario"].ToString();
                    loguin.email = reader["email_usuario"].ToString();


                }
            

            return loguin;
            


        }

        public void CrearTodoItem(TodoItem valor) {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");

            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"INSERT INTO `tarea` ( `actividad`, `fec_inicio`, `fec_vencida`, `id_user_tar`, `estado`)  VALUES (\"{valor.value}\",\"{valor.fec_inicio.ToString("yyyy/MM/dd")}\",\"{valor.fec_vencida.ToString("yyyy/MM/dd")}\",\"{valor.id_user_tar}\",\"{valor.estado}\") ";
            cmd.ExecuteNonQuery();
        }
        public TodoItem BuscarTodoItem(int id)
        {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");
            TodoItem rest = new TodoItem();
            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"SELECT * FROM `tarea` WHERE id_tarea= {id}";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                
                rest.id = int.Parse(reader["id_tarea"].ToString());
                rest.value = reader["actividad"].ToString();
                rest.fec_inicio = Convert.ToDateTime(reader["fec_inicio"].ToString());
                rest.fec_vencida = Convert.ToDateTime(reader["fec_vencida"].ToString());
                rest.fec_final = Convert.ToDateTime(reader["fec_finalizada"].ToString());
                rest.id_user_tar = int.Parse(reader["id_user_tar"].ToString());
                rest.estado = reader["estado"].ToString();
            }
            return rest;
        }

        public List<TodoItem> SelectTodoItem() {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");
            List<TodoItem> select = new List<TodoItem>();
            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"SELECT * FROM `tarea`";
            
            MySqlDataReader reader = cmd.ExecuteReader();
            
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    TodoItem item = new TodoItem();
                    item.id = int.Parse(reader["id_tarea"].ToString());
                    item.value = reader["actividad"].ToString();
                    item.fec_inicio = Convert.ToDateTime(reader["fec_inicio"].ToString());
                    item.fec_vencida = Convert.ToDateTime(reader["fec_vencida"].ToString());
                    item.fec_final = Convert.ToDateTime(reader["fec_finalizada"].ToString());
                    item.id_user_tar = int.Parse(reader["id_user_tar"].ToString());
                    item.estado = reader["estado"].ToString();

                    select.Add(item);

                    //select.Add(new TodoItem {id= int.Parse(reader["id"].ToString()), value= reader["value"].ToString() });
                    

                }
            }
            

            return select;
        }

        public TodoItem UpdateTodoItem(TodoItem valores)
        {
            TodoItem update = new TodoItem();
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");
            
            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"UPDATE `tarea` SET `actividad`=\"{valores.value}\", `fec_vencida`=\"{valores.fec_vencida.ToString("yyyy/MM/dd")}\",`estado`=\"{valores.estado}\"   WHERE `id_tarea`={valores.id }";
            cmd.ExecuteReader();

           
            return update;

        }

        public TodoItem UpdateStatusTodoItem(int id_tarea, string estado)
        {
            TodoItem update = new TodoItem();

            con.conectar("localhost", "do-or-die", "roota", "Adrian17");

            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"UPDATE `tarea` SET `estado`=\"{estado}\" WHERE `id_tarea`={id_tarea }";
            cmd.ExecuteReader();

            return update;

        }

        public TodoItem EliminarTodoItem(int id) {
            con.conectar("localhost", "do-or-die", "roota", "Adrian17");
            TodoItem delete = new TodoItem();
            MySqlCommand cmd = con.gateway.CreateCommand();
            cmd.CommandText = $"DELETE FROM `tarea` WHERE id_tarea= (\"{id}\") ";
            MySqlDataReader eliminar = cmd.ExecuteReader();
            
            
            return delete;
        }

        

    }
}
