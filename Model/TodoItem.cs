﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Model
{
    public class TodoItem
    {
        public int id { get; set; }
        public string value{ get; set; }
        public DateTime fec_inicio { get; set; }
        public DateTime fec_vencida { get; set; }
        public DateTime fec_final { get; set; }
        public int id_user_tar { get; set; }
        public string estado { get; set; }



    }
}
